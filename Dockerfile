FROM python

COPY . /tmp/.

WORKDIR /tmp

RUN pip install -r requirements.txt

ENTRYPOINT [ "python", "regex_filter.py"]