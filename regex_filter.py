from abc import ABC, abstractmethod
import argparse
import re
import sys
import logging  # Library to use a logger
import coloredlogs  # Library to colored output logs used with logging class


class bcolors:
    """
    Aux class to define colors to prompt in console.
    """
    OKGREEN = '\033[92m'
    ENDC = '\033[0m'


class Handler(ABC):
    """
    Main abstract class
    """

    def __init__(self, **kwargs):
        """
        Constructor. Called with input parameters.
        """
        self.regex = kwargs.get('regex')
        self.files = kwargs.get('files')
        self.input_text = kwargs.get('input_text')
        self.result = dict()

    def run(self):
        self._validate_text()
        self._output()

    def _validate_text(self):
        """Method to manage input text."""
        if self.files:
            self._read_file()
        else:
            self.result['STDIN'] = []
            self._validate_lines(self.input_text)

    def _read_file(self):
        """Method to read text in files."""
        for file in self.files:
            try:
                open_file = open(file, 'r')
                self.result[open_file.name] = []
                lines = open_file.readlines()
                if not lines:
                    sys.stdout.write(f"File {open_file.name} with empty text.\
                                     Nothing to validate\n\n")
                    continue
                self._validate_lines(lines, file_name=open_file.name)
            except FileNotFoundError:
                logging.error("FileNotFoundError: File {} not found.".format(
                                                                        file))
                raise
            finally:
                open_file.close()

    def _validate_lines(self, lines, file_name="STDIN"):
        """Method to validate regex for each line of input text"""
        for index, line in enumerate(lines):
            line_match = list(re.finditer(self.regex, line))
            if line_match:
                self.result[file_name].append({'line_number': index,
                                               'line': line,
                                               'line_match': line_match})

    @abstractmethod
    def _output(self):
        pass

    @abstractmethod
    def _decorate_output(self):
        pass


class NormalOutput(Handler):
    """
    Normal Output format.
    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def _output(self):
        line_control = -1
        for file, lines_props in self.result.items():
            for line_prop in lines_props:
                if line_control != line_prop['line_number']:
                    print('{} {} {}'.format(file, line_prop['line_number'],
                                            line_prop['line']))
                    line_control = line_prop['line_number']

    def _decorate_output(self):
        pass


class UnderlineOutput(Handler):
    """
    Underline Output format.
    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def _output(self):
        for file, lines_props in self.result.items():
            for line_prop in lines_props:
                max_len_line = 140
                line_prefix = '{} {}'.format(file, line_prop['line_number'])
                underline = self._decorate_output(line_prop)
                if len(line_prop['line']) < max_len_line:
                    print('{} {}'.format(line_prefix,
                                         line_prop['line'].strip()))
                    print('{} {}'.format(" " * len(line_prefix), underline))
                else:
                    logging.warning('Line length > {}. Output line will be '
                                    'splitted to display correctly the '
                                    'underline over the words that matchs the '
                                    'regex'.format(max_len_line))
                    chunks_line = [line_prop['line'][i:i+max_len_line]
                                   for i in range(0, len(line_prop['line']),
                                   max_len_line)]
                    chunks_underline = [underline[i:i+max_len_line]
                                        for i in range(0, len(underline),
                                        max_len_line)]
                    for n in range(len(chunks_line)):
                        print('{} {}'.format(line_prefix,
                                             chunks_line[n].strip()))
                        print('{} {}'.format(" " * len(line_prefix),
                                             chunks_underline[n]))

    def _decorate_output(self, line):
        output = " " * len(line['line'])
        for word_match in line['line_match']:
            output = output[:word_match.start()]\
                   + "^" * (word_match.end() - word_match.start())\
                   + output[word_match.end() + 1:]
        return output


class ColorOutput(Handler):
    """
    Color Output format.
    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def _output(self):
        for file, lines_props in self.result.items():
            for line_prop in lines_props:
                print('{} {} {}'.format(file, line_prop['line_number'],
                                        self._decorate_output(line_prop)))

    def _decorate_output(self, line):
        output = line['line']
        for word_match in line['line_match']:
            colored_word = f"{bcolors.OKGREEN}" + word_match.group()\
                         + f"{bcolors.ENDC}"
            output = output.replace(word_match.group(), colored_word,
                                    len(line['line_match']))
        return output


class MachineOutput(Handler):
    """
    Machine Output format.
    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def _output(self):
        for file, lines_props in self.result.items():
            for line_prop in lines_props:
                self._decorate_output(line_prop, file)

    def _decorate_output(self, line, file_name):
        for word_match in line['line_match']:
            print('{}:{}:{}:{}'.format(file_name, line['line_number'],
                                       word_match.start(), word_match.group()))


def regex(input):
    """Method to validate regex expression"""
    try:
        re.compile(input)
    except re.error:
        logging.error('Regex expression "{}" is not valid.'.format(input))
        raise ValueError
    return input


def parserArguments():
    """Method to parser arguments from command line"""
    parser = argparse.ArgumentParser(description='Filter a file by parsing \
                                     a regex expression.')
    mandatory_arg = parser.add_argument_group("Required arguments")
    mandatory_arg.add_argument('-r', '--regex', help='(mandatory) the regular \
                        expression to search for.', required=True, type=regex)
    parser.add_argument('-f', '--files', nargs='+', help='(optional) list of \
                        files to search in.', required=False)
    parser.add_argument('-u', '--underline', help='(optional) "^" is printed \
                        underneath the matched text.', required=False,
                        action='store_true')
    parser.add_argument('-c', '--color', help='(optional) the matched text is\
                        highlighted in color [1].', required=False,
                        action='store_true')
    parser.add_argument('-m', '--machine', help='(optional) print the output\
                         in the format: \
                        "file_name:line_number:start_position:matched_text".',
                        required=False, action='store_true')
    args = parser.parse_args()

    if not args.files:
        args.input_text = read_text_stdin()
    return args


def read_text_stdin():
    """Method to read text from stdin"""
    line_input = list()
    line_length = -1
    sys.stdout.write("Input text to validate and press ENTER when you "
                     "finish: \n")
    for line in sys.stdin:
        line_input.append(line)
        if line == "\n":
            if line_length >= 0:
                sys.stdout.write(("=" * line_length) + "\n\n")
                break
            else:
                sys.stdout.write("Empty input text. Nothing to validate\n")
                break
        line_length = len(line)
    return line_input


if __name__ == "__main__":
    coloredlogs.install()
    args = vars(parserArguments())
    if args["underline"]:
        UnderlineOutput(**args).run()
    elif args["color"]:
        ColorOutput(**args).run()
    elif args["machine"]:
        MachineOutput(**args).run()
    else:
        NormalOutput(**args).run()
