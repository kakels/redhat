# Filter text by passing a regex

This script searchs in text files or text input from STDIN for a regular expression input from parameters. The script's output format will be: "file_name line_number line" where the regular expression is matched.

## Parameters

- -r --regex       (mandatory): the regular expression to search for.
- -f --files       (optional): a list of files to search in. If this parameter is omitted, the script expects text input from STDIN.

These extra parameters are mutually exclusive. In case that more than one will be input, underline will have priority:

- -u, --underline  (optional): "^" is printed underneath the matched text.
- -c, --color      (optional): the matched text is highlighted in color [1].
- -m, --machine    (optional): print the output in the format: "file_name:line_number:start_position:matched_text".

## How to use it

Search in a single file and output format will be highlighted in color:

python regex_filter.py -r '([A-Z])\w+' -f </path/to/file> -c

Search in a multiple files and output format will print "^" under the matched text:

python regex_filter.py -r '([A-Z])\w+' -f </path/to/file1> </path/to/file2> </path/to/file3> -u

Search in text that will be requested to input in execution time and regular output will be prompted:

python regex_filter.py -r '([A-Z])\w+'


## Run in docker

1. Create the image, files text has to be in the same directory as the script folder:
- docker build -t regex_script .
2. Run the image with parameters:
- docker run regex_script <params>

Examples:

    1. Run script passing a file
    docker run regex_script -r '([A-Z])\w+' -f <file_name> <filename> -m

    1. Run script and input text by stdin
    docker run -it regex_script -r '([A-Z])\w+' -u 
    
